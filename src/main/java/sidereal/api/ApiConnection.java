package sidereal.api;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sidereal.objects.ConnectionServer;

@RestController
public class ApiConnection {

	
	private static final String serverNameQS = "serverName";
	private static final String serverNameQSDefault = "MyServer";
	
	private ArrayList<ConnectionServer> ConnectionServers = new ArrayList<ConnectionServer>();
	
	@RequestMapping(value = "/ConnectionServers", method = RequestMethod.POST)
    public ConnectionServer test(
    		HttpServletRequest request,
    		@RequestParam(value= serverNameQS , defaultValue= serverNameQSDefault ) String serverName)
    {
		
		// append port to address to create base path
		String serverAddress = request.getRemoteAddr()+":"+request.getRemotePort();
		
		for(int i=0;i<ConnectionServers.size();i++)
		{
			// found a server with the same name in the database, not adding again, and returning previously-added one
			if(ConnectionServers.get(i).getServerName().equals(serverName))
			{
				System.out.println("Not adding new server at address "+serverAddress+" as we already found it in our system");
				return ConnectionServers.get(i);
			}
		}
		
		ConnectionServer newServer = new ConnectionServer(serverName, serverAddress);
		ConnectionServers.add(newServer);
		
		System.out.println("Added new server at: "+newServer.getFullAddress()+" with name "+newServer.getServerName());
		
    	return newServer;
    }
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testGet ()
	{
		return "LALALALALA";
	}
	
}
