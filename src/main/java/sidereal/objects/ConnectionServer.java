package sidereal.objects;

public class ConnectionServer {

	private String serverName;
	private String fullAddress;
	
	public ConnectionServer(String serverName, String fullAddress)
	{
		this.serverName = serverName;
		this.fullAddress = fullAddress;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	
}
