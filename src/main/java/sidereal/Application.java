package sidereal;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableAutoConfiguration
public class Application {

	// set the mapping of the connection servers to register to.
	public static void main(String[] args)
	{
//		 start server
		SpringApplication.run(Application.class, args);
		
	}
	
}
